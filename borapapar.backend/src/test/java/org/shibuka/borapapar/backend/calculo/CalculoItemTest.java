package org.shibuka.borapapar.backend.calculo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO.Tipo;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class CalculoItemTest {

    @Test
    public void dadoCalculoItemSemPromocaoValorCalculadoDeveSerIgualPrecoMultiplicadoPelaQuantidade() {
        final ProdutoRestDTO dto = new ProdutoRestDTO();
        dto.id = "C8GDyLrHJb";
        dto.nome = "Amazing Salad!";
        dto.preco = bigDecimalFromDouble(500.00);

        final CalculoItem c = new CalculoItemSemPromocao();

        assertEquals(bigDecimalFromDouble(500.0), setScaleBigDecimal(c.calcular(dto, 1)));
        assertEquals(bigDecimalFromDouble(2500.0), setScaleBigDecimal(c.calcular(dto, 5)));
        assertEquals(bigDecimalFromDouble(5000.0), setScaleBigDecimal(c.calcular(dto, 10)));
    }

    @Test
    public void dadoCalculoItemPromocaoPercentualEntaoDeveAplicarDescontoSempre() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.percentual = 10;
        p.tipo = Tipo.FLAT_PERCENT;

        final ProdutoRestDTO dto = new ProdutoRestDTO();
        dto.id = "C8GDyLrHJb";
        dto.nome = "Amazing Salad!";
        dto.preco = bigDecimalFromDouble(499.00);

        final CalculoItem c = new CalculoItemPromocaoPercentual(p);


        assertEquals(bigDecimalFromDouble(449.10), setScaleBigDecimal(c.calcular(dto, 1)));
        assertEquals(bigDecimalFromDouble(2245.50), setScaleBigDecimal(c.calcular(dto, 5)));
        assertEquals(bigDecimalFromDouble(4491.00), setScaleBigDecimal(c.calcular(dto, 10)));
    }

    @Test
    public void dadoCalculoItemPromocaoSobreposicaoEntaoDeveAplicarApenasQuandoQuantidadeMaiorQueQuantidadeNecessaria() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.quantidadeNecessaria = 3;
        p.tipo = Tipo.QTY_BASED_PRICE_OVERRIDE;
        p.preco = bigDecimalFromDouble(2400.0);

        final ProdutoRestDTO dto = new ProdutoRestDTO();
        dto.id = "C8GDyLrHJb";
        dto.nome = "Amazing Salad!";
        dto.preco = bigDecimalFromDouble(500.00);

        final CalculoItem c = new CalculoItemPromocaoSobreposicao(p);

        assertEquals(bigDecimalFromDouble(500.00), setScaleBigDecimal(c.calcular(dto, 1)));
        assertEquals(bigDecimalFromDouble(1000.00), setScaleBigDecimal(c.calcular(dto, 2)));
        assertEquals(bigDecimalFromDouble(2400.0), setScaleBigDecimal(c.calcular(dto, 3)));
    }

    @Test
    public void dadoCalculoItemPromocaoSobreposicaoEntaoDeveAplicarPrecoDeveSerFixoIndependenteDaQuantidade() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.quantidadeNecessaria = 1;
        p.tipo = Tipo.QTY_BASED_PRICE_OVERRIDE;
        p.preco = bigDecimalFromDouble(2500.0);

        final ProdutoRestDTO dto = new ProdutoRestDTO();
        dto.id = "C8GDyLrHJb";
        dto.nome = "Amazing Salad!";
        dto.preco = bigDecimalFromDouble(500.00);

        final CalculoItem c = new CalculoItemPromocaoSobreposicao(p);

        assertEquals(bigDecimalFromDouble(2500.00), setScaleBigDecimal(c.calcular(dto, 1)));
        assertEquals(bigDecimalFromDouble(2500.00), setScaleBigDecimal(c.calcular(dto, 5)));
        assertEquals(bigDecimalFromDouble(2500.00), setScaleBigDecimal(c.calcular(dto, 10)));
        assertEquals(bigDecimalFromDouble(2500.00), setScaleBigDecimal(c.calcular(dto, 100)));  
    }

    @Test
    public void dadoCalculoItemPromocaoCompreXLeveYEntaoDeveAplicarApenasQuandoQuantidadeMaiorQueQuantidadeNecessaria() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.quantidadeNecessaria = 3;
        p.quantidadeGratis = 2;
        p.tipo = Tipo.BUY_X_GET_Y_FREE;
        p.preco = bigDecimalFromDouble(2400.0);

        final ProdutoRestDTO dto = new ProdutoRestDTO();
        dto.id = "C8GDyLrHJb";
        dto.nome = "Amazing Salad!";
        dto.preco = bigDecimalFromDouble(500.00);

        final CalculoItem c = new CalculoItemCompreXLeveY(p);

        assertEquals(bigDecimalFromDouble(500.00), setScaleBigDecimal(c.calcular(dto, 1)));
        assertEquals(bigDecimalFromDouble(1000.00), setScaleBigDecimal(c.calcular(dto, 2)));
        assertEquals(bigDecimalFromDouble(500.00), setScaleBigDecimal(c.calcular(dto, 3)));
    }

    @Test
    public void dadoCalculoItemPromocaoGanheXLeveYEntaoDeveAplicarRegraCorretamente() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.quantidadeNecessaria = 5;
        p.quantidadeGratis = 3;
        p.tipo = Tipo.BUY_X_GET_Y_FREE;
        p.preco = bigDecimalFromDouble(2500.0);

        final ProdutoRestDTO dto = new ProdutoRestDTO();
        dto.id = "C8GDyLrHJb";
        dto.nome = "Amazing Salad!";
        dto.preco = bigDecimalFromDouble(500.00);

        final CalculoItem c = new CalculoItemCompreXLeveY(p);

        assertEquals(bigDecimalFromDouble(1000.00), setScaleBigDecimal(c.calcular(dto, 5)));
        assertEquals(bigDecimalFromDouble(2000.00), setScaleBigDecimal(c.calcular(dto, 10)));
        assertEquals(bigDecimalFromDouble(3000.00), setScaleBigDecimal(c.calcular(dto, 15)));
        assertEquals(bigDecimalFromDouble(4000.00), setScaleBigDecimal(c.calcular(dto, 20)));  
    }    
    private BigDecimal bigDecimalFromDouble(Double valor) {
        return setScaleBigDecimal(BigDecimal.valueOf(valor));
    }

    private BigDecimal setScaleBigDecimal(BigDecimal bigDecimal){
        return bigDecimal.setScale(2);
    }
}
