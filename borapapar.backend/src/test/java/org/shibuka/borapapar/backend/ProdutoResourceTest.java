package org.shibuka.borapapar.backend;

import static io.restassured.RestAssured.given;

import javax.ws.rs.core.Response.Status;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;
import org.shibuka.borapapar.backend.configuration.WireMockProdutoExterno;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(WireMockProdutoExterno.class)
public class ProdutoResourceTest {
	private static final String ENDPOINT = "/produto";	
	private static final String ENDPOINT_ID = ENDPOINT + "/{idProduto}";
	private static final String ENDPOINT_CALCULAR = ENDPOINT_ID  + "/calcular/{quantidade}";

	@Test
	public void testGetEndpoint() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.when().get(ENDPOINT)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
	public void testGetByIdEndpoint() {
		final String resultado = given()
				.contentType(ContentType.JSON)
				.with().pathParam("idProduto", "Dwt5F7KAhi")
				.when()
				.get(ENDPOINT_ID).then()
				.statusCode(Status.OK.getStatusCode())
				.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

    @Test
    public void tesCalcularEndPoint() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.with().pathParam("idProduto", "Dwt5F7KAhi")
					.with().pathParam("quantidade", 2)
					.when().get(ENDPOINT_CALCULAR)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
    }
}