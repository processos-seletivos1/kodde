package org.shibuka.borapapar.backend;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.ws.rs.core.Response.Status;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;
import com.github.database.rider.core.api.dataset.DataSet;
import com.spun.util.ArrayUtils;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;
import org.shibuka.borapapar.backend.configuration.BackendTestLifecycle;
import org.shibuka.borapapar.backend.configuration.WireMockProdutoExterno;
import org.shibuka.borapapar.backend.domain.Carrinho;
import org.shibuka.borapapar.backend.domain.ItemCarrinho;
import org.shibuka.borapapar.backend.dto.interno.AdicionarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AdicionarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarItemCarrinhoDTO;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(BackendTestLifecycle.class)
@QuarkusTestResource(WireMockProdutoExterno.class)
public class CarrinhoResourceTest {
    private static final String ENDPOINT = "/carrinho";

    private static final String CENARIO_1_YML = "cenario-carrinho-tg-pacoca.yml";

    @Test
    @DataSet(CENARIO_1_YML)
	public void testGetEndPoint() {
		final String resultado =
				givenJson()
					.when().get(ENDPOINT)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

    @Test
    @DataSet(CENARIO_1_YML)
	public void testGetByIdEndPoint() {
		final String resultado =
				givenJson()
					.when().get(ENDPOINT + "/123")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

    @Test
    @DataSet(CENARIO_1_YML)
	public void testPostEndPoint() {
		final AdicionarItemCarrinhoDTO itemDTO = new AdicionarItemCarrinhoDTO();

		itemDTO.idProduto = "ABC123";
		itemDTO.quantidade = 10;

		final AdicionarCarrinhoDTO dto = new AdicionarCarrinhoDTO();

		dto.cliente = "Julia";
		dto.itens = ArrayUtils.asList(new AdicionarItemCarrinhoDTO[] { itemDTO });

        givenJson()
            .body(dto)
            .when().post(ENDPOINT)
            .then()
            .statusCode(Status.CREATED.getStatusCode());

        final Long quantidadeEsperada = 2L;
        final Long quantidadeAtual = Carrinho.count();

        assertEquals(quantidadeEsperada, quantidadeAtual);
	}

	@Test
    @DataSet(CENARIO_1_YML)
	public void testPutEndPoint() {
		final AtualizarItemCarrinhoDTO itemDTO = new AtualizarItemCarrinhoDTO();

		itemDTO.id = 321L;
		itemDTO.quantidade = 5;

		final AtualizarCarrinhoDTO dto = new AtualizarCarrinhoDTO();

		dto.itens = ArrayUtils.asList(new AtualizarItemCarrinhoDTO[] { itemDTO });

		givenJson()
            .with().pathParam("id", 123L)
            .body(dto)
            .when().put(ENDPOINT + "/{id}")
            .then()
            .statusCode(Status.NO_CONTENT.getStatusCode());

        final ItemCarrinho atual = ItemCarrinho.findById(itemDTO.id);

		assertTrue(itemDTO.quantidade.compareTo(atual.quantidade) == 0, "Quantidade deve ser atualizada após PUT");
    }

    @Test
    @DataSet(CENARIO_1_YML)
	public void testCheckoutEndPoint() {
		givenJson()
			.with().pathParam("id", 123L)
            .when().post(ENDPOINT+ "/{id}/checkout")
            .then()
            .statusCode(Status.CREATED.getStatusCode());
	}

	private RequestSpecification givenJson() {
		return given()
			.contentType(ContentType.JSON);
	}
}