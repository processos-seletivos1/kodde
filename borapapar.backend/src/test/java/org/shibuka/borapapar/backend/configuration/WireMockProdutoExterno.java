package org.shibuka.borapapar.backend.configuration;

import java.util.Collections;
import java.util.Map;

import com.github.tomakehurst.wiremock.WireMockServer;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WireMockProdutoExterno implements QuarkusTestResourceLifecycleManager {
	private WireMockServer wireMockServer;

	@Override
	public Map<String, String> start() {
		wireMockServer = new WireMockServer();
		wireMockServer.start();

		String jsonAllProducts = "[{\"id\":\"Dwt5F7KAhi\",\"name\":\"Amazing Pizza!\",\"price\":1099},{\"id\":\"PWWe3w1SDU\",\"name\":\"Amazing Burger!\",\"price\":999},{\"id\":\"C8GDyLrHJb\",\"name\":\"Amazing Salad!\",\"price\":499},{\"id\":\"4MB7UfpTQs\",\"name\":\"Boring Fries!\",\"price\":199}]";
		String jsonProductOverridePrice = "{\"id\":\"Dwt5F7KAhi\",\"name\":\"Amazing Pizza!\",\"price\":1099,\"promotions\":[{\"id\":\"ibt3EEYczW\",\"type\":\"QTY_BASED_PRICE_OVERRIDE\",\"required_qty\":2,\"price\":1799}]}";
		String jsonProductBuyXGetY = "{\"id\":\"PWWe3w1SDU\",\"name\":\"Amazing Burger!\",\"price\":999,\"promotions\":[{\"id\":\"ZRAwbsO2qM\",\"type\":\"BUY_X_GET_Y_FREE\",\"required_qty\":2,\"free_qty\":1}]}";
		String jsonProductFlatPercent = "{\"id\":\"C8GDyLrHJb\",\"name\":\"Amazing Salad!\",\"price\":499,\"promotions\":[{\"id\":\"Gm1piPn7Fg\",\"type\":\"FLAT_PERCENT\",\"amount\":10}]}";
		String jsonProduct = "{\"id\":\"4MB7UfpTQs\",\"name\":\"Boring Fries!\",\"price\":199,\"promotions\":[]}";
		String jsonProductPacoca = "{\"id\":\"ABC123\",\"name\":\"Paçoca Topzera\",\"price\":1.20,\"promotions\":[]}";
		
		stubFor(get(urlEqualTo("/products"))
			.willReturn(aResponse().withHeader("Content-Type", "application/json")
			.withBody(jsonAllProducts)));

		stubFor(get(urlEqualTo("/products/Dwt5F7KAhi"))
			.willReturn(aResponse().withHeader("Content-Type", "application/json")
			.withBody(jsonProductOverridePrice)));

		stubFor(get(urlEqualTo("/products/PWWe3w1SDU"))
			.willReturn(aResponse().withHeader("Content-Type", "application/json")
			.withBody(jsonProductBuyXGetY)));

		stubFor(get(urlEqualTo("/products/C8GDyLrHJb"))
			.willReturn(aResponse().withHeader("Content-Type", "application/json")
			.withBody(jsonProductFlatPercent)));
		
		stubFor(get(urlEqualTo("/products/ibt3EEYczW"))
			.willReturn(aResponse().withHeader("Content-Type", "application/json")
			.withBody(jsonProduct)));

		stubFor(get(urlEqualTo("/products/ABC123"))
			.willReturn(aResponse().withHeader("Content-Type", "application/json")
			.withBody(jsonProductPacoca)));			
			
			
		stubFor(get(urlMatching(".*")).atPriority(10).willReturn(aResponse().proxiedFrom("http://localhost:8081")));

		return Collections.singletonMap("org.shibuka.borapapar.backend.service.ProdutoExternoService/mp-rest/url",
				wireMockServer.baseUrl());
	}

	@Override
	public void stop() {
		if (null != wireMockServer) {
			wireMockServer.stop();
		}
	}
}
