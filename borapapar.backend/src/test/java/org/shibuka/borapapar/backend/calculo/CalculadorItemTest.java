package org.shibuka.borapapar.backend.calculo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import com.spun.util.ArrayUtils;

import org.junit.Before;
import org.junit.Test;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO.Tipo;
import org.shibuka.borapapar.backend.dto.interno.ItemCalculadoResponseDTO;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class CalculadorItemTest {

    private CalculadorItem calculadorItem;

    @Before
    public void setUp() {
        this.calculadorItem = new CalculadorItem();
    }

    @Test
    public void dadoCalculoUtilizandoCalculadorEntaoDeveRetornarDadosDoProduto() {
        final ProdutoRestDTO dtoProduto = new ProdutoRestDTO();
        dtoProduto.id = "C8GDyLrHJb";
        dtoProduto.nome = "Amazing Salad!";
        dtoProduto.preco = bigDecimalFromDouble(500.00);

        final ItemCalculadoResponseDTO dto = calculadorItem.calcular(dtoProduto, 1);

        assertEquals(dtoProduto.id, dto.idProduto);
        assertEquals(dtoProduto.nome, dto.nome);
        assertEquals(dtoProduto.preco, setScaleBigDecimal(dto.preco)); 
    }

    @Test
    public void dadoCalculoUtilizandoProdutoSemPromocaoEntaoNenhumaPromocaoDeveSerAplicada() {
        final ProdutoRestDTO dtoProduto = new ProdutoRestDTO();
        dtoProduto.id = "C8GDyLrHJb";
        dtoProduto.nome = "Amazing Salad!";
        dtoProduto.preco = bigDecimalFromDouble(500.00);

        final ItemCalculadoResponseDTO dto = calculadorItem.calcular(dtoProduto, 1);

        assertEquals(bigDecimalFromDouble(500.0), dto.totalSemDesconto);
        assertEquals(bigDecimalFromDouble(500.0), dto.totalComDesconto);
    }

    @Test
    public void dadoCalculoUtilizandoProdutoComPromocaoPercentualEntaoPromocaoDeveSerAplicada() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.percentual = 10;
        p.tipo = Tipo.FLAT_PERCENT;

        final ProdutoRestDTO dtoProduto = new ProdutoRestDTO();
        dtoProduto.id = "C8GDyLrHJb";
        dtoProduto.nome = "Amazing Salad!";
        dtoProduto.preco = bigDecimalFromDouble(500.00);
        dtoProduto.promocoes = ArrayUtils.asList(new PromocaoRestDTO[]{p});

        final ItemCalculadoResponseDTO dto = calculadorItem.calcular(dtoProduto, 10);

        assertEquals(bigDecimalFromDouble(4500.00), setScaleBigDecimal(dto.totalComDesconto));
        assertEquals(bigDecimalFromDouble(5000.00), setScaleBigDecimal(dto.totalSemDesconto));
    }

    @Test
    public void dadoCalculoUtilizandoProdutoComPromocaoSobreposicaoEntaoPromocaoDeveSerAplicada() {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.quantidadeNecessaria = 3;
        p.tipo = Tipo.QTY_BASED_PRICE_OVERRIDE;
        p.preco = bigDecimalFromDouble(2400.0);

        final ProdutoRestDTO dtoProduto = new ProdutoRestDTO();
        dtoProduto.id = "C8GDyLrHJb";
        dtoProduto.nome = "Amazing Salad!";
        dtoProduto.preco = bigDecimalFromDouble(500.00);
        dtoProduto.promocoes = ArrayUtils.asList(new PromocaoRestDTO[]{p});

        final ItemCalculadoResponseDTO dto = calculadorItem.calcular(dtoProduto, 10);

        assertEquals(bigDecimalFromDouble(2400.0), setScaleBigDecimal(dto.totalComDesconto));
        assertEquals(bigDecimalFromDouble(5000.0), setScaleBigDecimal(dto.totalSemDesconto));
    }

    @Test
    public void dadoCalculoUtilizandoProdutoComPromocaoCompreXGanheYEntaoPromocaoDeveSerAplicada () {
        final PromocaoRestDTO p = new PromocaoRestDTO();
        p.quantidadeNecessaria = 5;
        p.quantidadeGratis = 3;
        p.tipo = Tipo.BUY_X_GET_Y_FREE;

        final ProdutoRestDTO dtoProduto = new ProdutoRestDTO();
        dtoProduto.id = "C8GDyLrHJb";
        dtoProduto.nome = "Amazing Salad!";
        dtoProduto.preco = bigDecimalFromDouble(500.00);
        dtoProduto.promocoes = ArrayUtils.asList(new PromocaoRestDTO[]{p});

        final ItemCalculadoResponseDTO dto = calculadorItem.calcular(dtoProduto, 10);

        assertEquals(bigDecimalFromDouble(2000.0), setScaleBigDecimal(dto.totalComDesconto));
        assertEquals(bigDecimalFromDouble(5000.0), setScaleBigDecimal(dto.totalSemDesconto));
    }
    private BigDecimal bigDecimalFromDouble(Double valor) {
        return setScaleBigDecimal(BigDecimal.valueOf(valor));
    }

    private BigDecimal setScaleBigDecimal(BigDecimal bigDecimal){
        return bigDecimal.setScale(2);
    }
}
