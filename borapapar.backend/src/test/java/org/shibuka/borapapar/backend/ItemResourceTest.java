package org.shibuka.borapapar.backend;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.ws.rs.DELETE;
import javax.ws.rs.core.Response.Status;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;
import com.github.database.rider.core.api.dataset.DataSet;

import org.junit.jupiter.api.Test;
import org.shibuka.borapapar.backend.configuration.WireMockProdutoExterno;
import org.shibuka.borapapar.backend.domain.ItemCarrinho;
import org.shibuka.borapapar.backend.dto.interno.AdicionarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarItemCarrinhoDTO;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(WireMockProdutoExterno.class)
public class ItemResourceTest {
	private static final String ENDPOINT = "/carrinho/{idCarrinho}/item";

	private static final String CENARIO_1_YML = "cenario-carrinho-tg-pacoca.yml";

    @Test
    @DataSet(CENARIO_1_YML)
	public void testPostEndPoint() {
		final AdicionarItemCarrinhoDTO dto = new AdicionarItemCarrinhoDTO();

		dto.idProduto = "Dwt5F7KAhi";
		dto.quantidade = 2;

		givenJson()
			.with().pathParam("idCarrinho", 123L)
            .body(dto)
            .when().post(ENDPOINT)
            .then()
            .statusCode(Status.CREATED.getStatusCode());

			final Long quantidadeEsperada = 2L;
			final Long quantidadeAtual = ItemCarrinho.count();

			assertEquals(quantidadeEsperada, quantidadeAtual);
	}

	@Test
    @DataSet(CENARIO_1_YML)
	public void testPutEndPoint() {
		final AtualizarItemCarrinhoDTO dto = new AtualizarItemCarrinhoDTO();

		dto.id = 321L;
		dto.quantidade = 5;

		givenJson()
			.with().pathParam("idCarrinho", 123L)
			.with().pathParam("id", dto.id)
            .body(dto)
            .when().put(ENDPOINT + "/{id}")
            .then()
            .statusCode(Status.NO_CONTENT.getStatusCode());

        final ItemCarrinho atual = ItemCarrinho.findById(dto.id);

		assertTrue(dto.quantidade.compareTo(atual.quantidade) == 0, "Quantidade deve ser atualizada após PUT");
	}

	@DELETE
	@DataSet(CENARIO_1_YML)
	public void testDeleteEndPoint() {
		givenJson()
			.with().pathParam("idCarrinho", 123L)
			.with().pathParam("id", 321L)
			.when().delete(ENDPOINT + "/{id}")
			.then()
				.statusCode(Status.NO_CONTENT.getStatusCode());

		final Long quantidadeEsperada = 0L;
		final Long quantidadeAtual = ItemCarrinho.count();

		assertEquals(quantidadeEsperada, quantidadeAtual);
	}

	private RequestSpecification givenJson() {
		return given()
			.contentType(ContentType.JSON);
	}
}