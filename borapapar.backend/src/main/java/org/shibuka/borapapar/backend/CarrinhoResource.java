package org.shibuka.borapapar.backend;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.shibuka.borapapar.backend.dto.interno.AdicionarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.CarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.CarrinhoListDTO;
import org.shibuka.borapapar.backend.service.CarrinhoService;

@Path("/carrinho")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "carrinho")
public class CarrinhoResource {

    @Inject
    CarrinhoService carrinhoService;

    @GET
    public List<CarrinhoListDTO> findAll() {
        return carrinhoService.findAll();
    }

    @GET
    @Path("/{id}")
    public CarrinhoDTO findById(@PathParam("id") Long id) {
        return carrinhoService.findById(id);
    }

    @POST
    @Transactional
    public Response adicionar(AdicionarCarrinhoDTO dto) {
        final URI location = carrinhoService.adicionar(dto);

        return Response.created(location).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response atualizar(@PathParam("id") Long id, AtualizarCarrinhoDTO dto) {
        carrinhoService.atualizar(id, dto);

        return Response.noContent().build();
    }

    @POST
    @Path("/{id}/checkout")
    @Transactional
    public Response checkout(@PathParam("id") Long id) {
        final URI location = carrinhoService.checkout(id);
        
		return Response.created(location).build();
    }    
}