package org.shibuka.borapapar.backend;

import java.net.URI;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.shibuka.borapapar.backend.dto.interno.AdicionarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.service.ItemService;

@Path("/carrinho/{idCarrinho}/item")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "item")
@Tag(name = "carrinho")
public class ItemResource {

    @Inject
    ItemService itemService;

    @POST
    @Transactional
    public Response adicionar(
            @PathParam("idCarrinho") Long idCarrinho, 
        AdicionarItemCarrinhoDTO dto) {
        final URI location = itemService.adicionar(idCarrinho, dto);

        return Response.created(location).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response atualizar(
        @PathParam("idCarrinho") Long idCarrinho,
        @PathParam("id") Long id, 
        AtualizarItemCarrinhoDTO dto) {
        itemService.atualizar(id, dto);

        return Response.noContent().build();
    }
            
    @DELETE
    @Path("/{id}")    
    @Transactional
    public Response removerItem(
            @PathParam("idCarrinho") Long idCarrinho, 
            @PathParam("id") Long id
    ) {
        itemService.removerItem(idCarrinho, id);

        return Response.noContent().build();
    }    
}