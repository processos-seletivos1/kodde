package org.shibuka.borapapar.backend.dto.interno;

import java.util.List;

public class AdicionarCarrinhoDTO {
    public String cliente;

    public List<AdicionarItemCarrinhoDTO> itens;
}
