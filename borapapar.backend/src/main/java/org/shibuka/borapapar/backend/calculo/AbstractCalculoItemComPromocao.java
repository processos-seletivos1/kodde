package org.shibuka.borapapar.backend.calculo;

import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;

public abstract class AbstractCalculoItemComPromocao {

    protected final PromocaoRestDTO promocao;

    public AbstractCalculoItemComPromocao(PromocaoRestDTO p) {
        super();
        this.promocao = p;
    }

    protected boolean isQuantidadeNecessaria(int qtde) {
        return qtde >= promocao.quantidadeNecessaria;
    }
}
