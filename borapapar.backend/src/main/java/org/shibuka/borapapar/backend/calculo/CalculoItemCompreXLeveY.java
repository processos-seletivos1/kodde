package org.shibuka.borapapar.backend.calculo;

import java.math.BigDecimal;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;

public final class CalculoItemCompreXLeveY extends AbstractCalculoItemComPromocao implements CalculoItem {

    public CalculoItemCompreXLeveY(PromocaoRestDTO p) {
        super(p);
    }

    @Override
    public BigDecimal calcular(ProdutoRestDTO dto, int qtde) {
        return this.isQuantidadeNecessaria(qtde)
            ? calcularPrecoComPromocao(dto, BigDecimal.valueOf(qtde))
            : CalculoItem.super.calcular(dto, qtde);
    }

    private BigDecimal calcularPrecoComPromocao(ProdutoRestDTO dto, BigDecimal qtde) {
        final BigDecimal qtdeGratis 
            = qtde
                .multiply(BigDecimal.valueOf(promocao.quantidadeGratis))
                .divide(BigDecimal.valueOf(promocao.quantidadeNecessaria));
        final BigDecimal novaQtde = qtde.subtract(qtdeGratis);
        return dto.preco.multiply(novaQtde);
    }    
}

