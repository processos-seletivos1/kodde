package org.shibuka.borapapar.backend.service;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.shibuka.borapapar.backend.calculo.CalculadorItem;
import org.shibuka.borapapar.backend.domain.Carrinho;
import org.shibuka.borapapar.backend.domain.ItemCarrinho;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.interno.AdicionarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.ItemCalculadoResponseDTO;
import org.shibuka.borapapar.backend.dto.interno.ItemCarrinhoDTO;
import org.shibuka.borapapar.backend.mapper.ItemMapper;

@ApplicationScoped
public class ItemService {

    @Inject
    @RestClient
    ProdutoExternoService produtoService;

    @Inject
    ItemMapper itemMapper;

    public ItemCalculadoResponseDTO calcularItem(String idProduto, Integer quantidade) {
        final ProdutoRestDTO dtoProduto = produtoService.findProduto(idProduto);

        return new CalculadorItem().calcular(dtoProduto, quantidade);
    }

    public void atualizar(Long idCarrinho, AtualizarItemCarrinhoDTO dto) {
        final ItemCarrinho item = buscaItemCarrinho(dto.id);

        ItemCalculadoResponseDTO itemCalculado = this.calcularItem(item.idProduto, dto.quantidade);
        
        itemMapper.toItem(itemCalculado, item);

        item.persist();
    }    

    public URI adicionar(Long idCarrinho, AdicionarItemCarrinhoDTO dto) {
        final Carrinho carrinho = buscaCarrinho(idCarrinho);

        return adicionar(carrinho, dto);
    }
    
    public URI adicionar(Carrinho carrinho, AdicionarItemCarrinhoDTO dto) {
        ItemCalculadoResponseDTO itemCalculado = this.calcularItem(dto.idProduto, dto.quantidade);

        ItemCarrinho item = itemMapper.toItem(itemCalculado);
        
        item.carrinho = carrinho;

        item.persistAndFlush();

        return URI.create("/" + item.id);
    }

    public List<ItemCarrinhoDTO> findByCarrinho(Long id) {
        Stream<ItemCarrinho> itens = ItemCarrinho.stream("carrinho", buscaCarrinho(id));

        return itens
            .map(itemMapper::toDTO)
            .collect(Collectors.toList());
    }
    
    public void removerItem(Long idCarrinho, Long id) {
        final ItemCarrinho i = buscaItemCarrinho(idCarrinho, id);

        i.delete();        
    }


    private ItemCarrinho buscaItemCarrinho(Long idCarrinho, Long id) {
        final Carrinho c = buscaCarrinho(idCarrinho);
        final ItemCarrinho i = buscaItemCarrinho(id);

        if (c.id != i.carrinho.id) {
            throw new NotFoundException();
        }

        return i;            
    }

    private ItemCarrinho buscaItemCarrinho(Long id) {
        final Optional<ItemCarrinho> op = ItemCarrinho.findByIdOptional(id);

        if (op.isEmpty()) {
            throw new NotFoundException();
        }

        final ItemCarrinho item = op.get();
        return item;
    }    

    private Carrinho buscaCarrinho(Long id) {
        final Optional<Carrinho> op = Carrinho.findByIdOptional(id);

        if (op.isEmpty()) {
            throw new NotFoundException();
        }

        return op.get();
    }		
}
