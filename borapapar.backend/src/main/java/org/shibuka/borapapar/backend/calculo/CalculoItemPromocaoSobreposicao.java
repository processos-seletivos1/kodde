package org.shibuka.borapapar.backend.calculo;

import java.math.BigDecimal;

import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;

public final class CalculoItemPromocaoSobreposicao extends AbstractCalculoItemComPromocao implements CalculoItem {

    public CalculoItemPromocaoSobreposicao(PromocaoRestDTO p) {
        super(p);
    }

    @Override
    public BigDecimal calcular(ProdutoRestDTO dto, int qtde) {
        return isQuantidadeNecessaria(qtde)
            ? promocao.preco
            : CalculoItem.super.calcular(dto, qtde);
    }

}
