package org.shibuka.borapapar.backend.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "item_carrinho")
public class ItemCarrinho extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToOne
    public Carrinho carrinho;
    
    @Column(name = "id_produto")
    public String idProduto;
        
    public String nome;

    public BigDecimal preco;

    public Integer quantidade;    
    
    @Column(name = "total_sem_desconto")
    public BigDecimal totalSemDesconto;
    
    @Column(name = "total_com_desconto")
    public BigDecimal totalComDesconto;
}
