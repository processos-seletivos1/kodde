package org.shibuka.borapapar.backend.dto.externo;

import java.math.BigDecimal;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

public class ProdutoRestDTO {    
    public String id;

    @JsonbProperty("name")
    public String nome;

    @JsonbProperty("price")
    public BigDecimal preco;
    
    @JsonbProperty("promotions")
    public List<PromocaoRestDTO> promocoes;
}
