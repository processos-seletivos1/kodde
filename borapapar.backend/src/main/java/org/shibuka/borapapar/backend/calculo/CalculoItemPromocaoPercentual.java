package org.shibuka.borapapar.backend.calculo;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;

public final class CalculoItemPromocaoPercentual extends AbstractCalculoItemComPromocao implements CalculoItem {

	public CalculoItemPromocaoPercentual(PromocaoRestDTO p) {
		super(p);
	}

	@Override
	public BigDecimal calcular(ProdutoRestDTO dto, int qtde) {
		final BigDecimal preco = dto.preco.multiply(BigDecimal.valueOf(qtde));
		final BigDecimal percentual = BigDecimal
			.valueOf(100)
			.subtract(BigDecimal.valueOf(promocao.percentual))
			.divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_EVEN);
		;

		return preco.multiply(percentual);
	}
}
