package org.shibuka.borapapar.backend;

import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.shibuka.borapapar.backend.dto.interno.ItemCalculadoResponseDTO;
import org.shibuka.borapapar.backend.dto.interno.ProdutoDTO;
import org.shibuka.borapapar.backend.service.ItemService;
import org.shibuka.borapapar.backend.service.ProdutoService;

@Path("/produto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "produto")
public class ProdutoResource {

    @Inject
    ProdutoService service;
    
    @Inject
    ItemService itemService;

    @GET
    public List<ProdutoDTO> findAll() {
        return service.findAll();
    }

    @GET()
    @Path("/{id}")
    public ProdutoDTO getItem(@PathParam("id") String id) {
        final Optional<ProdutoDTO> op = service.findItem(id);

        if (op.isEmpty()) {
            throw new NotFoundException();
        }

        return op.get();
    }

    @GET
    @Path(value = "/{idProduto}/calcular/{quantidade}")
    @Tag(name = "item")
    public ItemCalculadoResponseDTO calcularItem(
            @PathParam("idProduto") String idProduto, 
            @PathParam("quantidade") Integer quantidade
        ) {
        return itemService.calcularItem(idProduto, quantidade);
    }    
}