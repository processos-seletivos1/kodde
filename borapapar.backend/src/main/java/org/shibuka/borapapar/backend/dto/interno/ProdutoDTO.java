package org.shibuka.borapapar.backend.dto.interno;

import java.math.BigDecimal;

public class ProdutoDTO {
    
    public String id;
    public String nome;
    public BigDecimal preco;

}
