package org.shibuka.borapapar.backend.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;

@Path("/products")
@RegisterRestClient(configKey = "produto-rest")
@Produces(MediaType.APPLICATION_JSON)
public interface ProdutoExternoService {

    @GET
    List<ProdutoRestDTO> findAll();

    @GET
    @Path("/{id}")
    ProdutoRestDTO findProduto(@PathParam("id") String id);

}
