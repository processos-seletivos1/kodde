package org.shibuka.borapapar.backend.calculo;

import java.util.Optional;

import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.externo.PromocaoRestDTO;
import org.shibuka.borapapar.backend.dto.interno.ItemCalculadoResponseDTO;

public class CalculadorItem {

    public ItemCalculadoResponseDTO calcular(ProdutoRestDTO dtoProduto, Integer qtde) {        
        final Optional<PromocaoRestDTO> opPromocao 
                = dtoProduto.promocoes == null 
                    ? Optional.empty()
                    : dtoProduto.promocoes.stream().findFirst();
        final CalculoItem calculoItemComPromocao = getCalculador(opPromocao);
        final CalculoItemSemPromocao calculoItemSemPromocao = new CalculoItemSemPromocao();
        
        final ItemCalculadoResponseDTO dto = new ItemCalculadoResponseDTO();

        dto.idProduto = dtoProduto.id;
        dto.nome = dtoProduto.nome;
        dto.quantidade = qtde;
        dto.preco = dtoProduto.preco;
        dto.totalSemDesconto = calculoItemSemPromocao.calcular(dtoProduto, qtde);
        dto.totalComDesconto = calculoItemComPromocao.calcular(dtoProduto, qtde);

        return dto;
    }

    private CalculoItem getCalculador(Optional<PromocaoRestDTO> op) {
        if (op.isEmpty()) {
            return new CalculoItemSemPromocao();
        }

        switch (op.get().tipo) {
            case BUY_X_GET_Y_FREE:
                return new CalculoItemCompreXLeveY(op.get());
            case FLAT_PERCENT:
                return new CalculoItemPromocaoPercentual(op.get());
            case QTY_BASED_PRICE_OVERRIDE:
                return new CalculoItemPromocaoSobreposicao(op.get());
            default:
                throw new PromocaoInvalidaException();
        }
    }
}
