package org.shibuka.borapapar.backend.dto.interno;

import java.util.List;

public class CarrinhoListDTO {

    public Long id;

    public String cliente;

    public List<ItemCarrinhoDTO> itens;
}
