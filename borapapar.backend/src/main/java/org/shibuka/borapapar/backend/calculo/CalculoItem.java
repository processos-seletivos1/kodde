package org.shibuka.borapapar.backend.calculo;

import java.math.BigDecimal;

import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;

public interface CalculoItem {
    default BigDecimal calcular(ProdutoRestDTO dto, int qtde) {
        return dto.preco.multiply(BigDecimal.valueOf(qtde));
    };

}
