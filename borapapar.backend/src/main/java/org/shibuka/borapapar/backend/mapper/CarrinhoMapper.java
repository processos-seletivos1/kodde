package org.shibuka.borapapar.backend.mapper;

import org.mapstruct.Mapper;
import org.shibuka.borapapar.backend.domain.Carrinho;
import org.shibuka.borapapar.backend.dto.interno.AdicionarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.CarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.CarrinhoListDTO;


@Mapper(componentModel = "cdi")
public interface CarrinhoMapper {    
    public CarrinhoDTO toDTO(Carrinho c);
    public CarrinhoListDTO toListDTO(Carrinho c);
    public Carrinho toCarrinho(AdicionarCarrinhoDTO dto);
}
