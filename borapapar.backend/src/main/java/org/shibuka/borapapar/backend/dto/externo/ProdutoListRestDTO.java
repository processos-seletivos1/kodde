package org.shibuka.borapapar.backend.dto.externo;

import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbProperty;

public class ProdutoListRestDTO {    
    public String id;

    @JsonbProperty("nome")
    public String nome;

    @JsonbProperty("price")
    public BigDecimal preco;    
}
