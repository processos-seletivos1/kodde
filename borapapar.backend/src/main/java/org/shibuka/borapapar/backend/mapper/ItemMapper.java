package org.shibuka.borapapar.backend.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.shibuka.borapapar.backend.domain.ItemCarrinho;
import org.shibuka.borapapar.backend.dto.externo.ProdutoListRestDTO;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.interno.ItemCalculadoResponseDTO;
import org.shibuka.borapapar.backend.dto.interno.ItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.ProdutoDTO;


@Mapper(componentModel = "cdi")
public interface ItemMapper {
    
    public ProdutoDTO toDTO(ProdutoRestDTO p);

    public ProdutoDTO toDTO(ProdutoListRestDTO p);
    
    public ItemCarrinhoDTO toDTO(ItemCarrinho i);

    public void toItem(ItemCalculadoResponseDTO itemCalculado, @MappingTarget ItemCarrinho i);

	public ItemCarrinho toItem(ItemCalculadoResponseDTO itemCalculado);
}
