package org.shibuka.borapapar.backend.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.shibuka.borapapar.backend.dto.externo.ProdutoRestDTO;
import org.shibuka.borapapar.backend.dto.interno.ProdutoDTO;
import org.shibuka.borapapar.backend.mapper.ItemMapper;

@ApplicationScoped
public class ProdutoService {

        @Inject
        @RestClient
        ProdutoExternoService produtoService;

        @Inject
        ItemMapper itemMapper;

        public List<ProdutoDTO> findAll() {
                return produtoService.findAll()
                        .stream()
                        .map(itemMapper::toDTO)                        
                        .collect(Collectors.toList());
        }

        public Optional<ProdutoDTO> findItem(String id) {
                final ProdutoRestDTO dto = produtoService.findProduto(id);
                if (dto == null) {
                        return Optional.empty();
                }                

                return Optional.of(itemMapper.toDTO(dto));
        }

}
