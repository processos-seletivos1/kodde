package org.shibuka.borapapar.backend.dto.externo;

import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbProperty;

public class PromocaoRestDTO {
    public String id;

    @JsonbProperty("type")
    public Tipo tipo;

    @JsonbProperty("required_qty")
    public Integer quantidadeNecessaria;
    
    // QTY_BASED_PRICE_OVERRIDE
    @JsonbProperty("price")
    public BigDecimal preco;

    // BUY_X_GET_Y_FREE
    @JsonbProperty("free_qty")
    public Integer quantidadeGratis;

    // FLAT_PERCENT
    @JsonbProperty("amount")
    public Integer percentual;    
    
    public enum Tipo {
        FLAT_PERCENT, BUY_X_GET_Y_FREE, QTY_BASED_PRICE_OVERRIDE;
    }        
}
