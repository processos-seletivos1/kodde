package org.shibuka.borapapar.backend.dto.interno;

import java.math.BigDecimal;

public class ItemCalculadoResponseDTO {
    public String idProduto;    
    public String nome;
    public Integer quantidade;
    public BigDecimal preco; 
    public BigDecimal totalSemDesconto;
    public BigDecimal totalComDesconto;
}
