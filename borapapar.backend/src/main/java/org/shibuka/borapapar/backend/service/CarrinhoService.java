package org.shibuka.borapapar.backend.service;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import org.shibuka.borapapar.backend.domain.Carrinho;
import org.shibuka.borapapar.backend.dto.interno.AdicionarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AdicionarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.AtualizarItemCarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.CarrinhoDTO;
import org.shibuka.borapapar.backend.dto.interno.CarrinhoListDTO;
import org.shibuka.borapapar.backend.mapper.CarrinhoMapper;

@ApplicationScoped
public class CarrinhoService {

    @Inject
    CarrinhoMapper carrinhoMapper;

    @Inject
    ItemService itemService;

    public List<CarrinhoListDTO> findAll() {
        Stream<Carrinho> s = Carrinho.streamAll();
        return s.map(carrinhoMapper::toListDTO).collect(Collectors.toList());
    }

    public CarrinhoDTO findById(Long id) {
        Optional<Carrinho> op = Carrinho.findByIdOptional(id);

        if (op.isEmpty()) {
            throw new NotFoundException();
        }

        final CarrinhoDTO dto = carrinhoMapper.toDTO(op.get());

        dto.itens = itemService.findByCarrinho(id);

        return dto;
    }

    public URI adicionar(AdicionarCarrinhoDTO dto) {
        Carrinho carrinho = carrinhoMapper.toCarrinho(dto);

        carrinho.persistAndFlush();

        for (AdicionarItemCarrinhoDTO itemDTO : dto.itens) {
            itemService.adicionar(carrinho, itemDTO);
        }

        return URI.create("/" + carrinho.id);
    }

    public void atualizar(Long id, AtualizarCarrinhoDTO dto) {
        for (AtualizarItemCarrinhoDTO itemDTO : dto.itens) {
            itemService.atualizar(id, itemDTO);
        }
    }

    public URI checkout(Long id) {
        return URI.create("/" + id);
    }
}
