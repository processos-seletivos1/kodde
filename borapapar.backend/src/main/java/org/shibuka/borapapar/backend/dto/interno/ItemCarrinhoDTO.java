package org.shibuka.borapapar.backend.dto.interno;

import java.math.BigDecimal;

public class ItemCarrinhoDTO {    
    public Long id;

    public String idProduto;
    
    public String nome;

    public BigDecimal preco;

    public Integer quantidade;
        
    public BigDecimal totalSemDesconto;
        
    public BigDecimal totalComDesconto;
}
