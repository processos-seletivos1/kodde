# kodde - BoraPapar

## Dependencies
- Java 11+
- Docker
- VueJS

## Instructions

```
# Terminal 1
» chmod +x ./start_backend.sh
» ./start_backend.sh

# Terminal 2
» chmod +x ./start_frontend.sh
» ./start_frontend.sh
```

## Backend

API documentation:
- http://localhost:8080/swagger-ui/

**Framework**:
- Quarkus

**Extensions:**
- quarkus-resteasy
- quarkus-resteasy-jsonb
- quarkus-hibernate-orm-panache
- quarkus-hibernate-validator
- quarkus-smallrye-openapi
- quarkus-jdbc-postgresql
- quarkus-flyway

**Others dependencies**
- Database Rider
- Test Containers
- Map Struct

**Questions:**

1. I used 16 hours to finish the backend and 4 hour to the frontend but didn't finished;
2. I would:
   - Code the frontend;
   - Add Validations to the REST API.
   - Add Jaeger;
   - Add JWT security setting up a local Keycloak authentication;
   - Add a CI/CD pipeline;
3. I would make promotions a object of the product or would add a priority field in the model;
4. Two things:
   - Configure the initial project and the dependencies;
   - Remember how to code the frontend, i didn't had the time;
5. I really enjoyed this test i think the freedom to choose the stack to use was really enjoyable.
